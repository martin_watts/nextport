import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';

import Index from '../pages/index';

test('The index page renders correctly', () => {
  const wrapper = shallow(<Index/>);

  expect(shallowToJson(wrapper)).toMatchSnapshot();
});
