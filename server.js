import {} from './config';
import { validationResult } from 'express-validator/check';
import compression from 'compression';
import express from 'express';
import i18NextMiddleware from 'i18next-express-middleware';
import next from 'next';
import NextI18Next from 'next-i18next';
import nextI18NextMiddleware from 'next-i18next/middleware';

import { messageValidators, contactFormHandler } from './components/server/contact';
import TRANSLATION_OPTIONS from './i18n';

TRANSLATION_OPTIONS.use = [i18NextMiddleware.LanguageDetector];

const DEFAULT_PORT = 3000;
const port = parseInt(process.env.PORT, 10) || DEFAULT_PORT;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const assetPrefix = process.env.SITE_PREFIX || '';

const HTTP_STATUS = {
  OK: 200,
  CLIENT_ERROR: 400,
  SERVER_ERROR: 500
};

if (assetPrefix) {
  app.setAssetPrefix(assetPrefix);
}

const createServer = () => {
  const server = express();

  nextI18NextMiddleware(new NextI18Next(TRANSLATION_OPTIONS), app, server);
  server.use(compression());
  server.use(express.json());

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.post('/contact', messageValidators, async(req, res) => {
    const errors = validationResult(req);

    if (errors.isEmpty()) {
      try {
        await contactFormHandler.sendMessage(req.body);
        res.sendStatus(HTTP_STATUS.OK);
      } catch (err) {
        res.sendStatus(HTTP_STATUS.SERVER_ERROR);
      }
    } else {
      res.status(HTTP_STATUS.CLIENT_ERROR);
      res.send(errors);
    }
  });
  return server;
};

const server = createServer();

if (!process.env.LAMBDA) {
  app.prepare()
    .then(() => {
      server.listen(port, (err) => {
        if (err) throw err;
        // eslint-disable-next-line
        console.log(`> Ready on http://localhost:${port}`);
      });
    });
}

exports.app = app;
exports.server = server;
