import { at, keys, pickBy } from 'lodash';
import { check } from 'express-validator/check';
import AWS from 'aws-sdk';
import fetch from 'isomorphic-unfetch';

// N.B. Have the option of replacing these with a logging framework later
// However, when deploying to AWS Lambda, CloudWatch picks up console logs
const error = process.env.TEST ? () => {} : console.error;
const info = process.env.TEST ? () => {} : console.info;

AWS.config.update({ region: process.env.AWS_SES_REGION || 'eu-west-1' });

const formFields = ['name', 'message', 'email', 'recaptcha'];
const recaptchaSecret = process.env.PRIVATE_RECAPTCHA_KEY;
const sendAddress = process.env.MESSAGE_SEND_ADDRESS;
const fromAddress = process.env.MESSAGE_FROM_ADDRESS;
const subject = process.env.MESSAGE_SUBJECT;
const NO_RECAPTCHA = 'NO_RECAPTCHA';

class ContactFormHandler {
  /**
   * @param {string} recaptchaValue The value of the recaptcha field from the contact form
   * @returns {string} The verification url
   * @memberof ContactFormHandler
   */
  getRecaptchaVerifyURL(recaptchaValue) {
    return `https://www.google.com/recaptcha/api/siteverify?secret=${recaptchaSecret}&response=${recaptchaValue}`;
  }

  /**
   * Calls the recaptcha validation url.
   * @param {string} recaptchaValue The value of the completed recaptcha control
   * @returns {Promise<void>} A Promise that resolves if the validation is successful, else rejects with an error.
   * @memberof ContactFormHandler
   */
  async validateRecaptcha (recaptchaValue) {
    if (!recaptchaSecret || recaptchaSecret === NO_RECAPTCHA) {
      return;
    }

    const response = await fetch(this.getRecaptchaVerifyURL(recaptchaValue), { method: 'POST' });
    if (!response.ok) {
      throw new Error(`Recaptcha validation failed - status code ${response.status}`);
    }

    const recaptchaResponse = await response.json();

    if (!recaptchaResponse.success) {
      throw new Error('Recaptcha validation failed');
    }
  }

  /**
   * Forwards the message from the contact form to an email recipient
   *
   * @param {*} senderName The name of the person sending the message
   * @param {*} senderEmail The email address of the person sending the message
   * @param {*} message The message content
   * @returns {Promise<void>} A promise that resolves if forwarding the message is successful, else rejects with an error.
   * @memberof ContactFormHandler
   */
  async forwardMesssage (senderName, senderEmail, message) {
    const params = {
      Destination: {
        ToAddresses: [sendAddress]
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: message
          },
          Text: {
            Charset: 'UTF-8',
            Data: message
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject || ''
        }
      },
      Source: fromAddress,
      ReplyToAddresses: [`${senderName} <${senderEmail}>`]
    };

    if (!recaptchaSecret || recaptchaSecret === NO_RECAPTCHA) {
      info(params);
      return Promise.resolve({ MessageId: 'NOT_SENT'});
    }

    // Create the promise and SES service object
    return await new AWS.SES({ apiVersion: '2010-12-01' })
      .sendEmail(params)
      .promise();
  }

  /**
   * Validates that the message form has been completed
   * @param {Object} messageForm An object containing the name, email, message, and recaptcha field values.
   * @memberof ContactFormHandler
   * @returns {void}
   */
  validateMessageForm(messageForm) {
    const [name, message, email, recaptcha] = at(messageForm, formFields);

    const missingParts = keys(
      pickBy({ name, message, email, recaptcha }, (value) => !value || !value.trim || value.trim() === ''),
    );

    if (missingParts.length > 0) {
      throw new Error(`The message form provided was not complete - Missing: ${missingParts.join(', ')}`);
    }
  }

  /**
   * @param {Object} messageForm An object containing the name, email, message, and recaptcha field values.
   * @returns {Promise<void>} A Promise that resolves when the message is sent successfully, or rejects on error
   */
  async sendMessage(messageForm) {
    if (!messageForm) {
      throw new Error('No message form provided');
    }
    messageForm.recaptcha = recaptchaSecret === NO_RECAPTCHA ? '_' : messageForm.recaptcha;

    this.validateMessageForm(messageForm);
    const [name, message, email, recaptcha] = at(messageForm, formFields);

    try {
      await this.validateRecaptcha(recaptcha);
    } catch (err) {
      error(err);
      throw new Error('There was a problem validating the recaptcha');
    }

    try {
      const sendResponse = await this.forwardMesssage(name, email, message);

      info(`Sent email: ${sendResponse.MessageId}`);
    } catch (err) {
      error(err);
      throw new Error('There was a problem sending the message');
    }
  }
}


/**
 * Validators that can be used to check a recieved message request is valid
 */
export const messageValidators = [
  check('name')
    .trim()
    .not()
    .isEmpty()
    .escape(),
  check('email')
    .trim()
    .isEmail(),
  check('message')
    .trim()
    .not()
    .isEmpty()
    .escape()
];

if (recaptchaSecret !== NO_RECAPTCHA) {
  messageValidators.push(
    check('recaptcha')
    .trim()
    .not()
    .isEmpty()
  );
}

export const contactFormHandler = new ContactFormHandler();
