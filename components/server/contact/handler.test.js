/* global fetchMock */

import { pick } from 'lodash';
import { validationResult } from 'express-validator/check';
import AWS from 'aws-sdk';
import fetch from 'isomorphic-unfetch';
import httpMocks from 'node-mocks-http';

import { mockRecaptchaSecret, mockFromAddress, mockSendAddress, mockSubject } from '../../../__mocks__/env';
import { messageValidators, contactFormHandler } from './handler';

let mockRequest;
let mockResponse;


beforeEach(() => {
  mockRequest = httpMocks.createRequest({
    method: 'POST',
    url: '/contact'
  });
  mockResponse = httpMocks.createResponse();
});

const validators = function* () {
  yield* messageValidators;
};

const runValidator = async validator => {
  return new Promise(resolve => {
    validator(mockRequest, mockResponse, resolve);
  });
};

const runValidators = async () => {
  const gen = validators();

  let validator = gen.next().value;

  while (validator) {
    await runValidator(validator);
    validator = gen.next().value;
  }
};

const mockMessageBody = {
  name: 'T. Ester',
  email: 'not@realadd.res',
  message: 'Testing, testing, 1,2,3',
  recaptcha: 'ABCD1234'
};

const setUpRequestWith = fields => {
  mockRequest.body = pick(mockMessageBody, fields);
};

describe('message request validation', () => {
  test(`rejects messages that don't contain contain a name`, async () => {
    // Arrange
    setUpRequestWith(['email', 'message', 'recaptcha']);

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'name',
        value: undefined,
        msg: 'Invalid value'
      }
    ]);
  });

  test(`rejects messages that contain a blank name`, async () => {
    // Arrange
    setUpRequestWith(['email', 'message', 'recaptcha']);
    mockRequest.body.name = ' ';

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'name',
        value: ' ',
        msg: 'Invalid value'
      }
    ]);
  });

  test(`rejects messages that don't contain a message`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'recaptcha']);

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'message',
        value: undefined,
        msg: 'Invalid value'
      }
    ]);
  });

  test(`rejects messages that contain a blank message`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'message', 'recaptcha']);
    mockRequest.body.message = ' ';

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'message',
        value: ' ',
        msg: 'Invalid value'
      }
    ]);
  });

  test(`rejects messages that don't contain an email address`, async () => {
    // Arrange
    setUpRequestWith(['name', 'message', 'recaptcha']);

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'email',
        value: undefined,
        msg: 'Invalid value'
      }
    ]);
  });

  test(`rejects messages that contain an invalid email address`, async () => {
    // Arrange
    const badEmail = 'bademail';
    setUpRequestWith(['name', 'message', 'recaptcha']);
    mockRequest.body.email = badEmail;

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'email',
        value: badEmail,
        msg: 'Invalid value'
      }
    ]);
  });

  test(`rejects messages that don't contain a recaptcha result code`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'message']);

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'recaptcha',
        value: undefined,
        msg: 'Invalid value'
      }
    ]);
  });

  test(`rejects messages that contain a blank recaptcha result code`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'message']);
    mockRequest.body.recaptcha = ' ';

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'recaptcha',
        value: ' ',
        msg: 'Invalid value'
      }
    ]);
  });

  test(`accepts messages that contain all required fields`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'message', 'recaptcha']);

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeTruthy();
  });
});

describe('send message', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
    fetchMock.resetMocks();
    fetchMock.mockResponse('{}');
  });

  afterAll(() => {
    fetch.resetMocks();
    jest.restoreAllMocks();
  });
  it('throws an exception if no message form is provided', async () => {
    try {
      // Act
      await contactFormHandler.sendMessage();
    } catch (err) {
      //Assert
      expect(err.message).toBe('No message form provided');
    }
  });

  test(`throws an exception if the provided message does not contain a name`, async () => {
    // Arrange
    setUpRequestWith(['email', 'message', 'recaptcha']);

    try {
      // Act
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      //Assert
      expect(err.message).toBe('The message form provided was not complete - Missing: name');
    }
  });

  test(`throws an exception if the provided message does notin a blank name`, async () => {
    // Arrange
    setUpRequestWith(['email', 'message', 'recaptcha']);
    mockRequest.body.name = ' ';

    try {
      // Act
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      //Assert
      expect(err.message).toBe('The message form provided was not complete - Missing: name');
    }
  });

  test(`throws an exception if the provided message does not contain a message`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'recaptcha']);

    try {
      // Act
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      //Assert
      expect(err.message).toBe('The message form provided was not complete - Missing: message');
    }
  });

  test(`throws an exception if the provided message does notin a blank message`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'message', 'recaptcha']);
    mockRequest.body.message = ' ';

    try {
      // Act
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      //Assert
      expect(err.message).toBe('The message form provided was not complete - Missing: message');
    }
  });

  test(`throws an exception if the provided message does not contain an email address`, async () => {
    // Arrange
    setUpRequestWith(['name', 'message', 'recaptcha']);

    // Act
    await runValidators();
    const errors = validationResult(mockRequest);

    // Assert
    expect(errors.isEmpty()).toBeFalsy();
    expect(errors.array()).toEqual([
      {
        location: 'body',
        param: 'email',
        value: undefined,
        msg: 'Invalid value'
      }
    ]);
  });

  test(`throws an exception if the provided message does not contain a email address`, async () => {
    // Arrange
    setUpRequestWith(['name', 'message', 'recaptcha']);

    try {
      // Act
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      //Assert
      expect(err.message).toBe('The message form provided was not complete - Missing: email');
    }
  });

  test(`throws an exception if the provided message does not contain a recaptcha result code`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'message']);

    try {
      // Act
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      //Assert
      expect(err.message).toBe('The message form provided was not complete - Missing: recaptcha');
    }
  });

  test(`throws an exception if the provided message does notin a blank recaptcha result code`, async () => {
    // Arrange
    setUpRequestWith(['name', 'email', 'message']);
    mockRequest.body.recaptcha = ' ';

    try {
      // Act
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      //Assert
      expect(err.message).toBe('The message form provided was not complete - Missing: recaptcha');
    }
  });

  test('validates the recaptcha then forwards the message', async () => {
    // Arrange
    jest.spyOn(contactFormHandler, 'validateRecaptcha').mockImplementation(() => Promise.resolve());
    jest.spyOn(contactFormHandler, 'forwardMesssage').mockImplementation(() =>
      Promise.resolve({
        MessageId: 'ABC123'
      }),
    );
    setUpRequestWith(['name', 'email', 'message', 'recaptcha']);

    // Act
    await contactFormHandler.sendMessage(mockRequest.body);

    // Assert
    expect(contactFormHandler.validateRecaptcha).toHaveBeenCalledWith(mockMessageBody.recaptcha);
    expect(contactFormHandler.forwardMesssage).toHaveBeenCalledWith(
      mockMessageBody.name,
      mockMessageBody.email,
      mockMessageBody.message,
    );
  });

  test('throws an exception if recaptcha validation fails ', async () => {
    // Arrange
    jest.spyOn(contactFormHandler, 'validateRecaptcha').mockImplementation(() => {
      throw new Error('Bad recaptcha');
    });
    jest.spyOn(contactFormHandler, 'forwardMesssage').mockImplementation(() =>
      Promise.resolve({
        MessageId: 'ABC123'
      }),
    );
    setUpRequestWith(['name', 'email', 'message', 'recaptcha']);

    // Act
    try {
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      // Assert
      expect(err.message).toBe('There was a problem validating the recaptcha');
      expect(contactFormHandler.forwardMesssage).not.toHaveBeenCalled();
    }
  });

  test('throws an exception if message sending fails ', async () => {
    // Arrange
    jest.spyOn(contactFormHandler, 'validateRecaptcha').mockImplementation(() => Promise.resolve());
    jest.spyOn(contactFormHandler, 'forwardMesssage').mockImplementation(() => {
      throw new Error('Forwarding fail');
    });
    setUpRequestWith(['name', 'email', 'message', 'recaptcha']);

    // Act
    try {
      await contactFormHandler.sendMessage(mockRequest.body);
    } catch (err) {
      // Assert
      expect(err.message).toBe('There was a problem sending the message');
    }
  });
});

describe('validating the recaptcha', () => {
  const restoreMocks = () => {
    jest.restoreAllMocks();
    fetchMock.resetMocks();
  };

  beforeEach(() => restoreMocks());

  afterAll(() => restoreMocks());

  test('throws an exception if the validation request returns a non-OK response code', async () => {
    // Arrange
    fetchMock.mockResponseOnce('Bad request', { status: 400 });

    try {
      // Act
      await contactFormHandler.validateRecaptcha(mockMessageBody.recaptcha);
    } catch (err) {
      //Assert
      expect(err.message).toBe('Recaptcha validation failed - status code 400');
    }
  });

  test('throws an exception if the validation attempt is unsuccessful', async () => {
    // Arrange
    fetchMock.mockResponseOnce('{"success": false}');

    try {
      // Act
      await contactFormHandler.validateRecaptcha(mockMessageBody.recaptcha);
    } catch (err) {
      //Assert
      expect(err.message).toBe('Recaptcha validation failed');
    }
  });

  test('does not throw an exception if the validation attempt is successful', async () => {
    // Arrange
    fetchMock.mockResponseOnce('{"success": true}');
    let errored = false;

    try {
      // Act
      await contactFormHandler.validateRecaptcha(mockMessageBody.recaptcha);
    } catch (err) {
      errored = true;
    }

    //Assert
    expect(errored).toBeFalsy();
  });

  test('sends the validation request to the correct endpoint', async () => {
    // Arrange
    fetchMock.mockResponseOnce('{"success": true}');

    try {
      // Act
      await contactFormHandler.validateRecaptcha(mockMessageBody.recaptcha);
    } catch (err) {
      // Don't let exceptions elsewhere in the handler cause false negatives in this test
    }

    //Assert
    const fetchCalls = fetchMock.mock.calls;

    expect(fetchCalls[fetchCalls.length - 1][0]).toBe(
      `https://www.google.com/recaptcha/api/siteverify?secret=${mockRecaptchaSecret}&response=${
        mockMessageBody.recaptcha
      }`,
    );
  });
});

describe('forwarding the message', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });

  test('sends the message via AWS SES', async () => {
    // Act
    await contactFormHandler.forwardMesssage(mockMessageBody.name, mockMessageBody.email, mockMessageBody.message);

    // Assert
    expect(AWS.SES).toHaveBeenCalledWith({ apiVersion: '2010-12-01' });
    expect(AWS.SES().sendEmail).toHaveBeenCalledWith({
      Destination: {
        ToAddresses: [mockSendAddress]
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: mockMessageBody.message
          },
          Text: {
            Charset: 'UTF-8',
            Data: mockMessageBody.message
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: mockSubject
        }
      },
      Source: mockFromAddress,
      ReplyToAddresses: [`${mockMessageBody.name} <${mockMessageBody.email}>`]
    });
  });
});

