import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import Hero, { TITLE_HIDE_LIMIT } from './hero';

/* global window, document, Event */

test('shows the hero title if the document is scrolled above the hide limit', () => {
  // Arrange
  document.scrollingElement = {
    scrollTop: TITLE_HIDE_LIMIT
  };

  // Act
  const wrapper = mount(<Hero/>);

  // Assert
  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot('title_visible');
});

test('hides the hero title if the document is scrolled below the hide limit', () => {
  // Arrange
  document.scrollingElement = {
    scrollTop: TITLE_HIDE_LIMIT + 1
  };

  // Act
  const wrapper = mount(<Hero/>);

  // Assert
  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot('title_hidden');
});

test('toggles the hero title show/hide on scroll', () => {
  // Arrange
  document.scrollingElement = {
    scrollTop: TITLE_HIDE_LIMIT + 1
  };

  const wrapper = mount(<Hero/>);

  document.scrollingElement.scrollTop = 0;

  // Act
  window.dispatchEvent(new Event('scroll'));
  wrapper.update();

  // Assert
  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot('title_visible');

  // Arrange
  document.scrollingElement.scrollTop = TITLE_HIDE_LIMIT + 1;

  // Act
  window.dispatchEvent(new Event('scroll'));
  wrapper.update();

  // Assert
  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot('title_hidden');
});
