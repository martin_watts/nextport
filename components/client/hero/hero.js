import get from 'lodash/get';
import PropTypes from 'prop-types';
import React from 'react';

import {Elem} from '../../../utils';
import css from '../../../sass';
import NextI18Next from '../../../i18n-client';

/* global window document*/

export const TITLE_HIDE_LIMIT = 50; // N.B: 50px is a bit arbitrary.
                             // It should really be a % based on the view width,
                             // but in practice it provideds acceptable behaviour.
/**
 * A Hero image with a centered title + sub-title
 *
 * @class Hero
 * @extends {React.Component}
 */
class Hero extends React.Component {
  /**
   * Creates an instance of Hero.
   * @param {*} props The component properties
   * @memberof Hero
   */
  constructor(props) {
    super(props);
    this.initialState = {
      showTitle: true
    };

    this.state = this.initialState;
    this.setInitialState = this.setInitialState.bind(this);
    this.setTitleShowHideState = this.setTitleShowHideState.bind(this);
  }

  /**
   * Sets the hero state so that the hero title is shown
   *
   * @returns {void}
   * @memberof Hero
   */
  setInitialState() {
    this.setState(this.initialState);
  }

  // @inheritdoc
  componentDidMount() {
    if (window) {
      window.addEventListener('scroll', this.setTitleShowHideState);
      this.setTitleShowHideState();
    }
  }

  /**
   * Show/hides the hero title depending on whether the document is scrolled above/below the hide limit
   *
   * @returns {void}
   * @memberof Hero
   */
  setTitleShowHideState() {
    const scrollTop = get(document, 'scrollingElement.scrollTop', 0);
    if (scrollTop > TITLE_HIDE_LIMIT) {
      this.setState({showTitle: false});
    } else {
      this.setInitialState();
    }
  }

  // @inheritdoc
  render() {
    const {t, forwardRef} = this.props;

    return (
      <section ref={forwardRef} className={css.hero}>
        <div className={[css.hero__title, this.state && this.state.showTitle ? 'show' : ''].join(' ')}>
          <h1 className={css.hero__name}>{t('hero.name')}</h1>
          <span className={css['hero__sub-title']}>{t('hero.jobTitle')}</span>
        </div>
      </section>
    );
  }
}

Hero.propTypes = {
  t: PropTypes.func,
  forwardRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Elem) })
  ])
};

const TranslatableHero = NextI18Next.withNamespaces('translation')(Hero);
const ForwardRefHero = React.forwardRef((props, ref) => <TranslatableHero {...props} forwardRef={ref} />);

export default ForwardRefHero;
