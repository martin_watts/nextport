import PropTypes from 'prop-types';

import markdownIt from 'markdown-it';
import markdownItContainer from 'markdown-it-container';
import markdownItReplaceLink from 'markdown-it-replace-link';
import parse from 'html-react-parser';

import {transformStaticLink } from '../../../utils';

const md = markdownIt({
  replaceLink: transformStaticLink
});
md.use(markdownItContainer, '*', {
  // Match any container name
  validate: (params) => params.trim().match(/^.*$/),
  render: (tokens, idx) => {
    const m = tokens[idx].info.trim().match(/^.*$/);

    if (tokens[idx].nesting === 1) {
      // opening tag
      return `<div class="${md.utils.escapeHtml(m.input)}">\n`;

    } else {
      // closing tag
      return '</div>\n';
    }
  }
});
md.use(markdownItReplaceLink);

/**
 * A component that renders a markdown string in a React component
 * @param {*} props The component props
 * @returns {string} The rendered markdown
 */
const Markdown = ({ source }) => {
  return parse(md.render(source.replace('\\n', '\n')));
};

Markdown.propTypes = {
  source: PropTypes.string
};

export default Markdown;
