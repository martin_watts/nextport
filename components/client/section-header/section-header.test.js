import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import SectionHeader from './section-header';

const title = 'Test Section';

test('The section header component renders correctly', () => {
  const wrapper = mount(<SectionHeader header={title}/>);

  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot();
});
