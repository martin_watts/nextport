import PropTypes from 'prop-types';
import React from 'react';

import css from '../../../sass';

const SectionHeader = props => (
  <header className={css['section-header']}>
    <h2>{props.header}</h2>
  </header>
);

SectionHeader.propTypes = {
  header: PropTypes.string
};

export default SectionHeader;
