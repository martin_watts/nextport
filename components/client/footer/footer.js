import Link from 'next/link';
import React from 'react';

import css from '../../../sass';

const Footer = () => (
  <footer className={css.footer}>
    <span className={css.footer__copyright}>&copy; Martin Watts, 2018-2019</span>
    <Link href="https://gitlab.com/martin_watts/nextport">
      <a className={css['footer__source-link']}>source available</a>
    </Link>
  </footer>
);

export default Footer;
