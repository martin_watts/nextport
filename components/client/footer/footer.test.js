import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import Footer from './footer';

test('The footer component renders correctly', () => {
  const wrapper = mount(<Footer/>);

  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot();
});
