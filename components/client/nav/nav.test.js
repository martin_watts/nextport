import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import Nav from './nav';

/* global document, window, Event */

const ref1 = React.createRef();
const ref2 = React.createRef();
ref1.current = document.createElement('div');
ref1.current.scrollIntoView = jest.fn();
ref2.current = null;
const testLinks = [{
  href: '#section1',
  translationKey: 'link1.title',
  key: 'section1',
  ref: ref1
}, {
  href: '#section2',
  translationKey: 'link2.title',
  key: 'section2',
  ref: ref2
}];
const previousSiblingHeight = 500;
let wrapper;
const testTitle = 'Doc Title';

beforeEach(() => {
  wrapper = mount(
    <div>
      <div id="hero"/>
      <Nav links={testLinks}/>
    </div>
  );
  document.title = testTitle;
});

test('is transparent when not sticky', async() => {
  // Arrange
  document.scrollingElement = {
    scrollTop: 0
  };

  const nav = wrapper.find('Nav').instance();
  jest.spyOn(nav, 'getStickyTop').mockImplementation(() => previousSiblingHeight + 1); //Fake being placed after a 500px high element

  // Act
  window.dispatchEvent(new Event('resize')); //Force sticky top recalculation as JSDOM results in everything having 0 height
  wrapper.update();


  // Assert
  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot('transparent');
});

test('Is solid when sticky', async() => {
  // Arrange
  document.scrollingElement = {
    scrollTop: previousSiblingHeight + 1
  };

  const nav = wrapper.find('Nav').instance();
  jest.spyOn(nav, 'getStickyTop').mockImplementation(() => previousSiblingHeight + 1);

  // Act
  window.dispatchEvent(new Event('resize'));
  wrapper.update();

  // Assert
  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot('solid');
});

test('Toggles between solid and transparent when the window scrolls', async() => {
  // Arrange
  document.scrollingElement = {
    scrollTop: previousSiblingHeight + 1
  };

  const nav = wrapper.find('Nav').instance();
  jest.spyOn(nav, 'getStickyTop').mockImplementation(() => previousSiblingHeight + 1);
  window.dispatchEvent(new Event('resize'));
  document.scrollingElement.scrollTop = previousSiblingHeight;

  // Act
  window.dispatchEvent(new Event('scroll'));
  wrapper.update();

  // Assert
  let navEl = wrapper.find('nav');
  expect(navEl.prop('className').indexOf('solid')).toBe(-1);

  // Arrange
  document.scrollingElement.scrollTop = previousSiblingHeight + 1;

  // Act
  window.dispatchEvent(new Event('scroll'));
  wrapper.update();

  navEl = wrapper.find('nav');
  expect(navEl.prop('className').indexOf('solid')).toBeGreaterThan(-1);
});

describe('scrolling to a link', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterAll(() => {
    jest.restoreAllMocks();
  });

  test('works when the clicked link has a referenced element to scroll to', () => {
    // Arrange
    const link1 = wrapper.find(`a[href="${testLinks[0].href}"]`);
    const event = {
      preventDefault: jest.fn()
    };
    jest.spyOn(window.history, 'pushState');

    // Act
    link1.simulate('click', event);

    // Assert
    expect(event.preventDefault).toHaveBeenCalled();
    expect(ref1.current.scrollIntoView).toHaveBeenCalledWith({behavior: 'smooth', block: 'start', inline: 'nearest'});
    expect(window.history.pushState).toHaveBeenCalledWith({}, testTitle, testLinks[0].href);
  });

  test('does not handle the event if the clicked link does not have a referenced element', () => {
    // Arrange
    const link2 = wrapper.find(`a[href="${testLinks[1].href}"]`);
    const event = {
      preventDefault: jest.fn()
    };
    jest.spyOn(window.history, 'pushState');

    // Act
    link2.simulate('click', event);


    // Assert
    expect(event.preventDefault).not.toHaveBeenCalled();
    expect(window.history.pushState).not.toHaveBeenCalled();
  });
});
