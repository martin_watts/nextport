import get from 'lodash/get';
import PropTypes from 'prop-types';
import React from 'react';
import Router from 'next/router';

import { Elem } from '../../../utils';
import css from '../../../sass';
import NextI18Next from '../../../i18n-client';

/* global window document*/

/**
 * The main site navigation
 *
 * @class Nav
 * @extends {React.Component}
 */
class Nav extends React.Component {
  constructor(props) {
    super(props);

    this.navRef = React.createRef();
    this.state = {
      solid: false
    };
    this.addRemoveSolidity = this.addRemoveSolidity.bind(this);
    this.stickyTop = 0;
  }

  // @inheritdoc
  componentDidMount() {
    if (!document || !document.scrollingElement) {
      return;
    }

    // The page might've loaded at an anchor point,
    // ensure the nav is rendered correctly for
    // the current scroll state.
    this.stickyTop = this.getStickyTop();
    this.addRemoveSolidity();

    window.addEventListener('scroll', this.addRemoveSolidity);
    window.addEventListener('resize', () => {
      this.stickyTop = this.getStickyTop();
      this.addRemoveSolidity();
    });
  }

  /**
   * Sets/unsets the nav's solidity depending on the scroll state of the document
   * @returns {void}
   * @memberof Nav
   */
  addRemoveSolidity() {
    const scrollTop = get(document, 'scrollingElement.scrollTop', 0);

    if (scrollTop < this.stickyTop) {
      this.setState(prevState => {
        return {
          ...prevState,
          solid: false
        };
      });
    } else {
      this.setState(prevState => {
        return {
          ...prevState,
          solid: true
        };
      });
    }
  }

  /**
   * Gets the offset from the top of the page at which the nav should become 'sticky'
   * The effect being attempted, is to have is the nav appearing to be statically positioned until
   * the page srolls past its top edge, at which point it sticks to the top of the viewport.
   *
   * @returns {Number} The offset from the page top at which the nav should become sticky
   * @memberof Nav
   */
  getStickyTop() {
    const nav = this.navRef && this.navRef.current;

    if (!nav || !window || !document || !document.scrollingElement || !nav.previousSibling) {
      return 0;
    } else {
      return nav.previousSibling.offsetTop + nav.previousSibling.offsetHeight;
    }
  }

  /**
   * Scrolls the browser to the referenced element
   * @param {object} event The link click event
   * @param {object} ref The React forward ref
   * @returns {void}
   */
  handleLinkClick(event, ref) {
    const el = ref.current || {};
    const targetHref = event.target.getAttribute('href');

    // If we can handle this ourselves, do so, otherwise let the browser take care of it
    if (!targetHref.indexOf('#') === 0) {
      Router.push(targetHref);
    } else if (el.scrollIntoView) {
      event.preventDefault();
      window.history.pushState({}, document.title, targetHref);

      el.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }
  }

  // @inheritdoc
  render() {
    const { t, links } = this.props;

    return (
      <section className={css.nav} ref={this.navRef}>
        <input id="main-menu-mobile-nav-toggle" type="checkbox" />
        <label className={css['main-menu__mobile-nav']} htmlFor="main-menu-mobile-nav-toggle" aria-label="Menu">
          <div className={[css.bar, css['bar-1']].join(' ')} />
          <div className={[css.bar, css['bar-2']].join(' ')} />
          <div className={[css.bar, css['bar-3']].join(' ')} />
        </label>
        <nav className={[css['main-menu'], this.state.solid ? css.solid : ''].join(' ')}>
          <ul className={css['main-menu__list']}>
            {links.map(link => (
              <li
                className={[css['main-menu__list-item'], links.indexOf(link) === links.length - 1 ? 'last' : ''].join(
                  ' ',
                )}
                key={link.key}
              >
                <a href={link.href} onClick={event => this.handleLinkClick(event, link.ref)}>{t(link.translationKey)}</a>
              </li>
            ))}
          </ul>
        </nav>
      </section>
    );
  }
}

Nav.propTypes = {
  t: PropTypes.func,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string,
      translationKey: PropTypes.string,
      key: PropTypes.string,
      ref: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({ current: PropTypes.instanceOf(Elem) })])
    }),
  )
};

export default NextI18Next.withNamespaces('translation')(Nav);
