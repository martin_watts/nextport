import fetch from 'isomorphic-unfetch';
import PropTypes from 'prop-types';
import React from 'react';
import ReCAPTCHAv2 from 'react-google-recaptcha';
import { loadReCaptcha as loadReCaptchaV3, ReCaptcha as ReCAPTCHAv3 } from 'react-recaptcha-v3';

import { Elem } from '../../../utils';
import css from '../../../sass';
import NextI18Next from '../../../i18n-client';
import Markdown from '../markdown';
import SectionHeader from '../section-header';

const contactUrl = process.env.SITE_PREFIX ? `/${process.env.SITE_PREFIX}/contact` : '/contact';

const HTTP_STATUS_OK = 200;
const NO_RECAPTCHA = 'NO_RECAPTCHA';
const RECAPTCHA_VERSION = {
  v2: 2,
  v3: 3
};

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.publicRecaptchaKey = process.env.PUBLIC_RECAPTCHA_KEY;
    this.recaptchaVersion = Number(process.env.RECAPTCHA_VERSION);
    this.initialState = {
      formComplete: false,
      recaptchaComplete: this.publicRecaptchaKey === NO_RECAPTCHA,
      name: '',
      email: '',
      message: '',
      sendError: false,
      sendSuccess: false,
      reCaptchaV3Token: ''
    };

    this.recaptchaRef = React.createRef();
    this.buttonRef = React.createRef();
    this.state = this.initialState;
    this.setInitialState = this.setInitialState.bind(this);
    this.checkFormComplete = this.checkFormComplete.bind(this);
    this.handleRecaptchaChange = this.handleRecaptchaChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmitMessage = this.handleSubmitMessage.bind(this);
    this.onRecaptchaV3Verify = this.onRecaptchaV3Verify.bind(this);
  }

  //@inheritdoc
  componentDidMount() {
    if (this.publicRecaptchaKey !== NO_RECAPTCHA && this.recaptchaVersion === RECAPTCHA_VERSION.v3) {
      loadReCaptchaV3(this.publicRecaptchaKey);
    }
  }

  /**
   * Blanks inputs and resets the recaptcha
   * @returns {void}
   * @memberof Contact
   */
  setInitialState() {
    this.setState(prevState => {
      return {
        ...this.initialState,
        reCaptchaV3Token: prevState.reCaptchaV3Token
      };
    });

    if (this.recaptchaRef.current && this.recaptchaRef.current.reset) {
      this.recaptchaRef.current.reset();
    }
  }

  /**
   * Sets the formComplete flag to reflect the current component state
   * @returns {void}
   * @memberof Contact
   */
  checkFormComplete() {
    this.setState(prevState => {
      return {
        ...prevState,
        formComplete: !!prevState.name && !!prevState.email && !!prevState.message && prevState.recaptchaComplete
      };
    });
  }

  /**
   * Handles form input, updates the component state
   *
   * @param {object} event The change event of a form element
   * @returns {void}
   * @memberof Contact
   */
  handleChange(event) {
    const { value, name } = event.target;
    if (name) {
      this.setState(prevState => {
        const newState = {
          ...prevState
        };
        newState[name] = value;

        return newState;
      }, this.checkFormComplete);
    }
  }

  /**
   * Called when the user completes the Recaptcha challenge
   * @returns {void}
   * @memberof Contact
   */
  handleRecaptchaChange() {
    this.setState(prevState => {
      return {
        ...prevState,
        recaptchaComplete: true
      };
    }, this.checkFormComplete);
  }

  /**
   * Called when the v3 ReCAPTCHA completes validation
   * @param {string} token The validation token to send to the backend
   * @returns {void}
   * @memberof Contact
   */
  onRecaptchaV3Verify(token) {
    this.setState(prevState => {
        return {
        ...prevState,
        reCaptchaV3Token: token
      };
    }, this.handleRecaptchaChange);
  }

  /**
   * Sends the form contents to the server and handles the response
   *
   * @param {object} event The form submit event
   * @returns {void}
   * @memberof Contact
   */
  handleSubmitMessage(event) {
    event.preventDefault();

    fetch(contactUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        message: this.state.message,
        recaptcha: this.state.reCaptchaV3Token || (this.recaptchaRef.current && this.recaptchaRef.current.getValue())
      })
    })
      .then(res => {
        if (res.status !== HTTP_STATUS_OK) {
          this.setState(prevState => {
            return {
              ...prevState,
              sendError: true
            };
          });
        } else {
          this.setInitialState();
          this.setState(prevState => {
            return {
              ...prevState,
              sendSuccess: true
            };
          });
        }
      })
      .catch(err => {
        this.setState(prevState => {
          return {
            ...prevState,
            sendError: true
          };
        });
        console.error(err);
      });
  }

  // @inheritdoc
  render() {
    const { t, forwardRef } = this.props;
    const content = t('contact.content', { returnObjects: true, defaultValue: [] });

    return (
      <section ref={forwardRef} id="contact" className={css.contact}>
        <SectionHeader header={t('contact.header')} />
        <div className={css.content}>
          {content.map((c, idx) => (
            <Markdown source={c} key={`contact-content-${idx}`}/>
          ))}
        </div>
        <div className={[css.form, css['contact__form-wrapper']].join(' ')}>
          {this.state.sendError ? <div className={css['form-error']}>{t('contact.error')}</div> : null}
          {this.state.sendSuccess ? <div className={css['form-success']}>{t('contact.success')}</div> : null}
          <form className={css.form} onSubmit={this.handleSubmitMessage}>
            <div className={[css['form-field'], css.first].join(' ')}>
              <label htmlFor="name">
                <span className={css['form-field-label']}>{t('contact.name.label')}</span>
                <input id="name" type="text" name="name" value={this.state.name} onChange={this.handleChange} />
              </label>
            </div>
            <div className={css['form-field']}>
              <label htmlFor="email">
                <span className={css['form-field-label']}>{t('contact.email.label')}</span>
                <input id="email" type="email" name="email" value={this.state.email} onChange={this.handleChange} />
              </label>
            </div>
            <div className={css['form-field']}>
              <label htmlFor="message">
                <span className={css['form-field-label']}>{t('contact.message.label')}</span>
                <textarea id="message" name="message" value={this.state.message} onChange={this.handleChange} />
              </label>
            </div>
            {this.publicRecaptchaKey !== NO_RECAPTCHA && this.recaptchaVersion === RECAPTCHA_VERSION.v3 ? (
              <ReCAPTCHAv3
                sitekey={this.publicRecaptchaKey}
                action="contact"
                verifyCallback={this.onRecaptchaV3Verify}
              />
            ) : null}
            {this.publicRecaptchaKey !== NO_RECAPTCHA && this.recaptchaVersion === RECAPTCHA_VERSION.v2 ? (
              <ReCAPTCHAv2
                className={css['form-recapture']}
                ref={this.recaptchaRef}
                sitekey={this.publicRecaptchaKey}
                onChange={this.handleRecaptchaChange}
                size="normal"
              />
            ) : null}
            <button
              disabled={!this.state.formComplete}
              ref={this.buttonRef}
              type="submit"
              className={[css.button, css['button--primary']].join(' ')}
            >
              {t('contact.submit')}
            </button>
          </form>
        </div>
      </section>
    );
  }
}

Contact.propTypes = {
  t: PropTypes.func,
  forwardRef: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({ current: PropTypes.instanceOf(Elem) })])
};

const TranslatableContact = NextI18Next.withNamespaces('translation')(Contact);
const ForwardRefContact = React.forwardRef((props, ref) => <TranslatableContact {...props} forwardRef={ref} />);

export default ForwardRefContact;
