import { last } from 'lodash';
import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import {} from '../../../__mocks__/env';
import Contact from './form';

/* global fetchMock */

let wrapper;
let contactInstance;
const forwardRef = React.createRef();
const testName = 'A. Tester';
const testEmail = 'a@tes.ter';
const testMessage = `1,2,1,2
                     This is 
                     Just 
                     A
                     Test`;
const testRecaptchaValue = 'ABC123';
let nameField;
let emailField;
let messageField;
let recaptcha;
let sendButton;
const formSuccessMessageSelector = '.form-success';
const formErrorMessageSelector = '.form-error';

const checkButtonState = enabled => {
  contactInstance.forceUpdate();
  wrapper.update();
  sendButton = wrapper.find('button');

  if (enabled) {
    expect(sendButton.prop('disabled')).toBeFalsy();
  } else {
    expect(sendButton.prop('disabled')).toBeTruthy();
  }
};

describe('The contact form component', () => {
  beforeEach(() => {
    wrapper = mount(<Contact ref={forwardRef} />);
    contactInstance = wrapper.find('Contact').instance();
    nameField = wrapper.find('#name');
    emailField = wrapper.find('#email');
    messageField = wrapper.find('#message');
    recaptcha = wrapper.find('ReCAPTCHA');
    sendButton = wrapper.find('button');
  });

  test('renders correctly in its default state', () => {
    expect(mountToJson(wrapper, { mode: 'deep' })).toMatchSnapshot();
  });

  describe('when its input fields change', () => {

    test('enables the send button when the form is complete', async () => {
      // Act
      nameField.instance().value = testName;
      nameField.simulate('change', testName);

      // Assert
      checkButtonState(/* enabled= */ false);

      // Act
      emailField.instance().value = testEmail;
      emailField.simulate('change', testEmail);

      // Assert
      checkButtonState(/* enabled= */ false);

      // Act
      messageField.instance().value = testMessage;
      messageField.simulate('change', testMessage);

      // Assert
      checkButtonState(/* enabled= */ false);

      // Act
      // recaptcha.simulate('change') does not work (?)
      recaptcha.instance().props.onChange();

      // Assert
      checkButtonState(/* enabled= */ true);
    });
  });

  describe('submitting the form', () => {
    let form;
    const contactUrl = '/contact';

    const restoreMocks = () => {
      jest.restoreAllMocks();
      fetchMock.resetMocks();
      fetchMock.mockResponseOnce('{}');
      contactInstance.setState({});
    };

    beforeEach(() => {
      restoreMocks();
      form = wrapper.find('form');
    });

    afterAll(() => restoreMocks());

    test('stops the default browser behaviour', () => {
      // Arrange
      const preventDefault = jest.fn();

      // Act
      form.simulate('submit', { preventDefault });

      //Assert
      expect(preventDefault).toHaveBeenCalled();
    });

    test('posts the form state to the contact endpoint', () => {
      // Arrange
      contactInstance.setState({
        name: testName,
        email: testEmail,
        message: testMessage
      });
      contactInstance.recaptchaRef.current = { getValue: () => testRecaptchaValue, reset: jest.fn() };

      // Act
      form.simulate('submit');

      //Assert
      const lastFetchArgs = last(fetchMock.mock.calls);

      expect(lastFetchArgs).toEqual([
        contactUrl,
        {
          body: JSON.stringify({
            name: testName,
            email: testEmail,
            message: testMessage,
            recaptcha: testRecaptchaValue
          }),
          headers: { 'Content-Type': 'application/json' },
          method: 'POST'
        }
      ]);
    });

    test('resets its form and shows a success message if the post is successful', async () => {
      // Arrange
      contactInstance.setState({
        name: testName,
        email: testEmail,
        message: testMessage,
        recaptchaComplete: true,
        formComplete: true
      });
      contactInstance.recaptchaRef.current = { getValue: () => testRecaptchaValue, reset: jest.fn() };

      // Assert
      expect(wrapper.exists(formSuccessMessageSelector)).toBeFalsy();

      // Act
      form.simulate('submit');
      const lastFetchResult = last(fetchMock.mock.results);

      await lastFetchResult.value; // Allow the fetch response handler to run

      //Assert
      checkButtonState(/*enabled=*/ false);
      expect(wrapper.exists(formSuccessMessageSelector)).toBeTruthy();
      expect(contactInstance.recaptchaRef.current.reset).toHaveBeenCalled();
    });

    test('updates its error state if it recieves a not-OK code', async () => {
      // Arrange
      fetchMock.resetMocks();
      fetchMock.mockResponseOnce('Bad request', { status: 400 });
      contactInstance.setState({
        name: testName,
        email: testEmail,
        message: testMessage,
        recaptchaComplete: true,
        formComplete: true
      });
      contactInstance.recaptchaRef.current = { getValue: () => testRecaptchaValue, reset: jest.fn() };

      // Assert
      expect(wrapper.exists(formErrorMessageSelector)).toBeFalsy();

      // Act
      form.simulate('submit');
      const lastFetchResult = last(fetchMock.mock.results);
      await lastFetchResult.value; // Allow the fetch response handler to run

      //Assert
      checkButtonState(/*enabled=*/ true);
      expect(wrapper.exists(formErrorMessageSelector)).toBeTruthy();
      expect(contactInstance.recaptchaRef.current.reset).not.toHaveBeenCalled();
    });

    test('updates its error state if it errors for another reason', async () => {
        // Arrange
        fetchMock.resetMocks();
        fetchMock.mockRejectOnce('ERR_NETWORK_CHANGED');
        contactInstance.setState({
          name: testName,
          email: testEmail,
          message: testMessage,
          recaptchaComplete: true,
          formComplete: true
        });
        contactInstance.recaptchaRef.current = { getValue: () => testRecaptchaValue, reset: jest.fn() };
        console.error = jest.fn();

        // Assert
        expect(wrapper.exists(formErrorMessageSelector)).toBeFalsy();

        // Act
        try {
          form.simulate('submit');
          const lastFetchResult = last(fetchMock.mock.results);
          await lastFetchResult.value; // Allow the fetch response handler to run
        } catch (e) {
          await Promise.resolve(); //Exceptional case, allow the catch to occur in the component as well
        }

        //Assert
        checkButtonState(/*enabled=*/ true);
        expect(wrapper.exists(formErrorMessageSelector)).toBeTruthy();
        expect(contactInstance.recaptchaRef.current.reset).not.toHaveBeenCalled();
        expect(console.error).toHaveBeenCalledWith('ERR_NETWORK_CHANGED');
    });
  });
});
