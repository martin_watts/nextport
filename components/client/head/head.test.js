import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import React from 'react';

import Head from './head';

test('The head component renders correctly', () => {
  const wrapper = shallow(<Head title="NextPort" description="A single page Next.js portfolio-ready site"/>);

  expect(shallowToJson(wrapper)).toMatchSnapshot();
});
