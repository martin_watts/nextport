import React from 'react';
import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';

import About from './about';

test('The about section renders correctly', () => {
  const wrapper = mount(<About/>);

  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot();
});
