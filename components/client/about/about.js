import PropTypes from 'prop-types';
import React from 'react';

import { Elem } from '../../../utils';
import css from '../../../sass';
import NextI18Next from '../../../i18n-client';
import Markdown from '../markdown';
import SectionHeader from '../section-header';

const About = ({t, forwardRef}) => {
  const content = t('about.content', {returnObjects: true, defaultValue: []});

  return (
    <section ref={forwardRef} id="about" className={[css.about, css.flexgrid].join(' ')}>
      <SectionHeader header={t('about.header')} />
      {content.map((c, idx) => <Markdown source={c} key={`about-content${idx}`} />)}
    </section>
  );
};

About.propTypes = {
  t: PropTypes.func,
  forwardRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Elem) })
  ])
};

const TranslatableAbout = NextI18Next.withNamespaces('translation')(About);
const ForwardRefAbout = React.forwardRef((props, ref) => <TranslatableAbout {...props} forwardRef={ref}/>);

export default ForwardRefAbout;
