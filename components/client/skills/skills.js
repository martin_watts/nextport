import PropTypes from 'prop-types';
import React from 'react';

import {Elem, transformStaticLink} from '../../../utils';
import css from '../../../sass';
import NextI18Next from '../../../i18n-client';
import Markdown from '../markdown';
import SectionHeader from '../section-header';

const Skills = ({t, forwardRef}) => {
  const content = t('skills.content', {returnObjects: true, defaultValue: []});
  const skillList = t('skills.list', {returnObjects: true, defaultValue: []});

  return (
    <div>
      <section ref={forwardRef} id="skills" className={css.flexgrid}>
        <SectionHeader header={t('skills.header')} />
        {content.map((c, idx) => <Markdown source={c} key={`skill-content-${idx}`} />)}
        <div className={css['skills-list']}>
          {
            skillList.map(skill => (
              <div className={css.skill} key={`skill-${skill.name}`}>
                <img src={transformStaticLink(skill.logo)} alt={skill.name} />
              </div>
            ))
          }
        </div>
      </section>
    </div>
  );
};

Skills.propTypes = {
  t: PropTypes.func,
  forwardRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Elem) })
  ])
};


const TranslatableSkills = NextI18Next.withNamespaces('translation')(Skills);
const ForwardRefSkills = React.forwardRef((props, ref) => <TranslatableSkills {...props} forwardRef={ref} />);

export default ForwardRefSkills;
