import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import React from 'react';

import Skills from './skills';

test('The skills component renders correctly', () => {
  const wrapper = mount(<Skills/>);

  expect(mountToJson(wrapper, {mode: 'deep'})).toMatchSnapshot();
});
