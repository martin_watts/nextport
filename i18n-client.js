import LanguageDetector from 'i18next-browser-languagedetector';
import NextI18Next from 'next-i18next';
import TRANSLATION_OPTIONS from './i18n';

TRANSLATION_OPTIONS.use = [LanguageDetector];

const i18n = new NextI18Next(TRANSLATION_OPTIONS);
export default i18n;
