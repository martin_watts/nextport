import { transformStaticLink } from './utils';

const options = {
  fallbackLng: 'en',
  defaultNS: 'translation',
  otherLanguages: ['en'],
  backend: {
    loadPath: transformStaticLink('/static/locales/{{lng}}/{{ns}}.json')
  }
};

export default options;
