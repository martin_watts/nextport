module.exports = {
  setupFiles: ['<rootDir>/jest.setup.js'],
  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'],
  moduleNameMapper: {
    'aws-sdk': '<rootDir>/__mocks__/aws-sdk',
    '(.+)styles.scss': 'identity-obj-proxy',
    '(.+)i18n-client': '<rootDir>/__mocks__/i18n-client',
    'isomorphic-unfetch': '<rootDir>/__mocks__/isomorphic-unfetch'
  },
  snapshotSerializers: ['enzyme-to-json/serializer']
};
