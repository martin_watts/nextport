// Node doesn't have an Element class, expose a shim to allow SSR + PropTypes to work
/* global Element*/
const Elem = (typeof Element === 'undefined' ? function() {} : Element);


const staticPrefix = process.env.SITE_PREFIX ? `/${process.env.SITE_PREFIX}` : '';

/**
 * Transforms links to the static directory if it's not deployed at the root of the domain
 * @param {string} url The static URL to transform
 * @returns {string} The (potentially) transformed URL
 */
const transformStaticLink = (url) => {
  if (url && url.indexOf('/static/') === 0) {
    return `${staticPrefix}${url}`;
  }

  return url;
};


module.exports = {
  Elem,
  transformStaticLink
};
