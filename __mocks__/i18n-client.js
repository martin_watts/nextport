import get from 'lodash/get';
import translations from './translations';


export default {
  withNamespaces: (/*namespaces*/) => (Component) => {
    Component.defaultProps = { ...Component.defaultProps,
      t: (key, options) => {
        const defaultValue = options && options.defaultValue ? options.defaultValue : key;

        return get(translations, key, defaultValue);
      }
    };
    return Component;
  }
};
