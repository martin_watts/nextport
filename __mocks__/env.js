import {assign} from 'lodash';

export const mockRecaptchaSiteKey = 'PUB_RECAP_KEY';
export const mockRecaptchaSecret = 'RECAP_KEY';
export const mockSendAddress = 'no@tr.eal';
export const mockFromAddress = 'also@notr.eal';
export const mockSubject = 'Test Message';

assign(process.env, {
  PUBLIC_RECAPTCHA_KEY: mockRecaptchaSiteKey,
  PRIVATE_RECAPTCHA_KEY: mockRecaptchaSecret,
  MESSAGE_SEND_ADDRESS: mockSendAddress,
  MESSAGE_FROM_ADDRESS: mockFromAddress,
  MESSAGE_SUBJECT: mockSubject,
  RECAPTCHA_VERSION: 2,
  TEST: true
});
