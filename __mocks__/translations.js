const translations = {
  link1: {
    title: 'Link 1'
  },
  link2: {
    title: 'Link 2'
  },
  page: {
    title: 'Testing NextPort'
  },
  hero: {
    name: 'Your Name Here',
    jobTitle: 'Test Wrangler'
  },
  nav: {
    home: 'Home',
    about: 'About',
    skills: 'Skills',
    contact: 'Contact'
  },
  about: {
    header: 'About Me',
    content: [
      'Testing, testing',
      '1. One',
      '2. Two',
      '3. Three'
    ]
  },
  skills: {
    header: 'My Skills',
    content: [
      'Maybe testing, maybe not'
    ],
    list: [
      {
        name: 'Jest',
        logo: '/static/img/jest.png'
      },
      {
        name: 'Jasmine',
        logo: '/static/img/jasmine.png'
      }
    ]
  },
  contact: {
    header: 'Contact Me',
    content: [
      'Don\'t use this form, it\'s just a test'
    ],
    name: {
      label: 'Your Name'
    },
    email: {
      label: 'Email'
    },
    message: {
      label: 'Message'
    },
    submit: 'Send',
    error: 'Message failed to send',
    success: 'Message sent'
  }
};

export default translations;
