const mockSES = {
  sendEmail: jest.fn().mockImplementation(() => {
    return {
      promise: () => Promise.resolve()
    };
  })
};

const mockSdk = {
  config: {
    update: jest.fn()
  },
  SES: jest.fn().mockImplementation(() => mockSES)
};

export default mockSdk;
