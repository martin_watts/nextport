//Load environment variables
const dotEnvPath = process.env.NODE_ENV === 'production' ? './prod.env' : './dev.env';
const result = require('dotenv').config({path: dotEnvPath});
const { transformStaticLink } = require('./utils');
const PRETTY_PRINT_INDENT = 2;

if (result.error) {
  throw result.error;
}

console.log(JSON.stringify(result, null, PRETTY_PRINT_INDENT));

const withSass = require('@zeit/next-sass');
const DotenvWP = require('dotenv-webpack');
const sass = require('node-sass');

module.exports = withSass({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: '[local]'
  },
  sassLoaderOptions: {
    functions: {
      'staticUrl($url)': (url) => {
        const urlValue = transformStaticLink(url.getValue());
        return sass.types.String(urlValue);
      }
    }
  },
  webpack: (config) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    };

    const plugins = config.plugins || [];

    // Allow the client bundle to access environment variables as defined at compile time.
    // We could use Next.js's env config option, but that exposes _all_ of the .env variables,
    // some of which may be private.
    plugins.push(new DotenvWP({
      path: dotEnvPath
    }));

    config.plugins = plugins;

    return config;
  }
});
