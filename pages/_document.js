import Document, { Head, Main, NextScript } from 'next/document';
import React from 'react';

import css from '../sass';

export default class MyDocument extends Document {
  render() {
    return (
      <html lang="en-GB" className={css.html}>
        <Head/>
        <body id="home" className={css.body}>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
