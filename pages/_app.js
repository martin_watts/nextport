import App, { Container } from 'next/app';
import React from 'react';

import { Head, Layout } from '../components/client';
import NextI18Next from '../i18n-client';

class NextPort extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    pageProps.namespacesRequired = ['translation'];

    return { pageProps };
  }

  render() {
    const { Component, pageProps, t } = this.props;

    return (
      <Container>
        <Head title={t('page.title')} />
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Container>
    );
  }
}

const TranslatableApp = NextI18Next.withNamespaces('translation')(NextPort);
export default NextI18Next.appWithTranslation(TranslatableApp);
