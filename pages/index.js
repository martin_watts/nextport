import React from 'react';

import { About, Contact, Footer, Hero, Nav, Skills } from '../components/client';

const [homeRef, aboutRef, skillsRef, contactRef] = [React.createRef(), React.createRef(), React.createRef(), React.createRef()];

const links = [
  {
    href: '#home',
    translationKey: 'nav.home',
    ref: homeRef
  },
  {
    href: '#about',
    translationKey: 'nav.about',
    ref: aboutRef
  },
  {
    href: '#skills',
    translationKey: 'nav.skills',
    ref: skillsRef
  },
  {
    href: '#contact',
    translationKey: 'nav.contact',
    ref: contactRef
  }
].map(link => {
  link.key = `nav-link-${link.href}-${link.translate_key}`;
  return link;
});

export default () => (
  <main className="grid">
    <Hero ref={homeRef}/>
    <Nav links={links}/>
    <About ref={aboutRef}/>
    <Skills ref={skillsRef}/>
    <Contact ref={contactRef}/>
    <Footer/>
  </main>
);
