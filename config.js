import fs from 'fs-extra';
import path from 'path';
import dotenv from 'dotenv';

const dotEnvPath = path.resolve(__dirname, process.env.NODE_ENV === 'production' ? 'prod.env' : 'dev.env');

if (fs.existsSync(dotEnvPath)) {
  dotenv.config({path: dotEnvPath});
} else {
  console.info(`No .env file found at ${dotEnvPath}`);
  process.env.PUBLIC_RECAPTCHA_KEY = process.env.PUBLIC_RECAPTCHA_KEY || 'NO_RECAPTCHA';
}
