import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as fetchMock from 'jest-fetch-mock';

configure({ adapter: new Adapter() });
global.fetchMock = fetchMock.default;
